
# Mixins

Place your app specific mixins in this directory.

For details about mixins, see https://vuejs.org/v2/guide/mixins.html
