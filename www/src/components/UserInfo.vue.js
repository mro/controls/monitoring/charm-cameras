// @ts-check

import Vue from "vue";
import { mapGetters } from "vuex";

/**
 * @typedef {V.Instance<typeof component>} Instance
 */
const component = Vue.extend({
  name: "UserInfo",
  computed: {
    /** @return {Iface.User} */
    user() { return this.$store?.state?.user; },
    .../** @type { isOperator(): boolean, isExpert(): boolean } */mapGetters([ "isOperator", "isExpert" ])
  }
});
export default component;
