// @ts-check
const
  Server = require('./Server');

const config = require('./config');

var server = new Server(config);

if (require.main === module) {
  (async function() {
    /* we're called as a main, let's listen */
    await server.listen(() => {
      console.log(`Server listening on port http://localhost:${config.port}`);
    });
  }());

}
else {
  /* export our server */
  module.exports = server;
}
